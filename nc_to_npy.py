#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 15:20:27 2020

@author: arndt
"""


import numpy as np
import glob
import os
import sys
import netCDF4 as nc4

try:
    day = sys.argv[1]
except Exception:
    day = "*"
if "mistral" in os.getcwd():
    work = "/mnt/work"
else:
    work = "/mnt/lustre02/work/bd1083/b309177"

rad = ['ev_250_aggr1km_refsb_1', 'ev_250_aggr1km_refsb_2', 'ev_1km_emissive_29', 'ev_1km_emissive_33', 'ev_1km_emissive_34', 'ev_1km_emissive_35', 'ev_1km_emissive_36', 'ev_1km_refsb_26', 'ev_1km_emissive_27', 'ev_1km_emissive_20', 'ev_1km_emissive_21', 'ev_1km_emissive_22', 'ev_1km_emissive_23','latitude', 'longitude']
properties = ['cloud_water_path', 'cloud_optical_thickness', 'cloud_effective_radius', 'cloud_phase_optical_properties', 'cloud_top_pressure', 'cloud_top_height', 'cloud_top_temperature', 'cloud_emissivity', 'surface_temperature']
rois = 'cloud_mask'
labels = 'cloud_layer_type'
dicts=[rad,  properties, rois, labels]

MAX_WIDTH, MAX_HEIGHT = 1354, 2030

# -------------------------------------------------------------------------------------------------- UTILS

def get_tile_offsets(tile_size):

    offset = tile_size // 2
    offset_2 = offset

    if not tile_size % 2:
        offset_2 -= 1

    return offset, offset_2

def get_sampling_mask(mask_shape=(MAX_HEIGHT, MAX_WIDTH), tile_size=3):
    """ returns a mask of allowed centers for the tiles to be sampled. The center of an even size tile is considered to be the point at the position (size // 2 + 1, size // 2 + 1) within the tile.
    """
    mask = np.ones(mask_shape, dtype=np.uint8)

    offset, offset_2 = get_tile_offsets(tile_size)

    # must not sample tile centers in the borders, so that tiles keep to required shape
    mask[:, :offset] = 0
    mask[:, -offset_2:] = 0
    mask[:offset, :] = 0
    mask[-offset_2:, :] = 0

    return mask

def get_label_mask(labels):
    """ given the class occurences channels over 10 layers, returns a 2d array marking as 1 the labelled pixels and as 0 the unlabelled ones."""

    label_mask = np.sum(~labels.mask, 3) > 0

    return label_mask 

def get_unlabel_mask(label_mask, tile_size=3):
    """returns inverse of label mask, with all pixels around a labelled one eroded."""

    offset, offset_2 = get_tile_offsets(tile_size)

    unlabel_mask = (~label_mask).copy()

    labelled_idx = np.where(label_mask)

    for center_w, center_h in zip(*labelled_idx):

        w1 = center_w - offset
        w2 = center_w + offset_2 + 1
        h1 = center_h - offset
        h2 = center_h + offset_2 + 1

        unlabel_mask[w1:w2, h1:h2] = False

    return unlabel_mask


# -------------------------------------------------------------------------------------------------- SAMPLERS

def sample_cloudy_unlabelled_tiles(swath_tuple, cloud_mask, label_mask, number_of_tiles, tile_size=3):
    """
    :param swath_tuple: 
    :param cloud_mask: 2d array of zise (w, h) marking the cloudy pixels 
    :param label_mask: 2d array of zise (w, h) marking the labelled pixels 
    :param number_of_tiles: the number of tiles to sample. It is reset to maximal number of tiles that can be sampled, if bigger
    :param tile_size: size of the tile selected from within the image
    :return: a 4-d array (nb_tiles, nb_channels, w, h) of sampled tiles; and a list of tuples ((w1, w2), (h1, h2)) with the relative positions of the sampled tiles withing the swath
    The script will use a cloud_mask channel to mask away all non-cloudy data and a label_mask channel to mask away all labelled data. The script will then randomly select a number of tiles (:param number of tiles) from the cloudy areas that are unlabelled.
    """

    # mask out borders not to sample outside the swath
    allowed_pixels = get_sampling_mask((MAX_WIDTH, MAX_HEIGHT), tile_size)

    # mask out labelled pixels and pixels around them
    unlabel_mask = get_unlabel_mask(label_mask)

    # combine the three masks, tile centers will be sampled from the cloudy and unlabelled pixels that are not in the borders of the swath
    unlabelled_pixels = np.logical_and.reduce([allowed_pixels, cloud_mask, unlabel_mask])
    unlabelled_pixels_idx = np.where(unlabelled_pixels == 1)
    unlabelled_pixels_idx = list(zip(*unlabelled_pixels_idx))

    number_of_tiles = min(number_of_tiles, len(unlabelled_pixels_idx))

    # sample without replacement
    tile_centers_idx = np.random.choice(np.arange(len(unlabelled_pixels_idx)), number_of_tiles, False)
    unlabelled_pixels_idx = np.array(unlabelled_pixels_idx)
    tile_centers = unlabelled_pixels_idx[tile_centers_idx]
    
    # compute distances from tile center of tile upper left and lower right corners
    offset, offset_2 = get_tile_offsets(tile_size)

    positions, tiles = [], [[] for _ in swath_tuple]
    for center in tile_centers:
        center_w, center_h = center

        w1 = center_w - offset
        w2 = center_w + offset_2 + 1
        h1 = center_h - offset
        h2 = center_h + offset_2 + 1

        tile_position = ((w1, w2), (h1, h2))

        positions.append(tile_position)   
        for i, a in enumerate(swath_tuple):     
            tiles[i].append(a[:, w1:w2, h1:h2])

    positions = np.stack(positions)
    for i, t in enumerate(tiles):     
        tiles[i] = np.stack(t)

    return tiles, positions


def extract_cloudy_labelled_tiles(swath_tuple, cloud_mask, label_mask, tile_size=3):
    """
    :param swath_tuple: input numpy array from MODIS of size (nb_channels, w, h)
    :param cloud_mask: 2d array of zise (w, h) marking the cloudy pixels 
    :param label_mask: 2d array of zise (w, h) marking the labelled pixels 
    :param tile_size: the size of the channels
    :return: a 4-d array (nb_tiles, nb_channels, w, h) of sampled tiles; and a list of tuples ((w1, w2), (h1, h2)) with the relative positions of the extracted tiles withing the swath
    The script will use a cloud_mask channel to mask away all non-cloudy data and a label_mask channel to mask away all unlabelled data. The script will then select all tiles from the cloudy areas that are labelled.
    """

    # mask not to sample outside the swath
    allowed_pixels = get_sampling_mask((MAX_WIDTH, MAX_HEIGHT), tile_size)

    # combine the three masks, tile centers will be sampled from the cloudy and labelled pixels that are not in the borders of the swath
    labelled_pixels = allowed_pixels & cloud_mask & label_mask
    labelled_pixels_idx = np.where(labelled_pixels == 1)
    w=np.copy(labelled_pixels_idx)
    labelled_pixels_idx = list(zip(*labelled_pixels_idx))
    print(len(np.where(label_mask==1)[0]), len(labelled_pixels_idx))
    if len(labelled_pixels_idx)==0:
        raise Exception
    
    offset, offset_2 = get_tile_offsets(tile_size)
    
    positions, tiles = [], [[] for _ in swath_tuple]
    for center in labelled_pixels_idx:
        center_w, center_h = center

        w1 = center_w - offset
        w2 = center_w + offset_2 + 1
        h1 = center_h - offset
        h2 = center_h + offset_2 + 1

        tile_position = ((w1, w2), (h1, h2))
        
        positions.append(tile_position) 
        for i, a in enumerate(swath_tuple):     
            tiles[i].append(a[:, w1:w2, h1:h2])

    positions = np.stack(positions)
    for i, t in enumerate(tiles):     
        tiles[i] = np.stack(t).astype("float16")
    
    return tiles  , positions.astype("float16")  

def sample_labelled_and_unlabelled_tiles(swath_tuple, cloud_mask, label_mask, tile_size=3):
    """
    :param swath_tuple: tuple of numpy arrays of size (C, H, W, ...) to be tiled coherently
    :param tile_size: size of tile (default 3)
    :param cloud_mask: mask where cloudy
    :param label_mask: mask where labels are available 
    :return: nested list of labelled tiles, unlabelled tiles, labelled tile positions, unlabelled tile positions
    Samples the same amount of labelled and unlabelled tiles from the cloudy data.
    """
        
    labelled_tiles, labelled_positions = extract_cloudy_labelled_tiles(swath_tuple, cloud_mask, label_mask, tile_size)

    number_of_labels = len(labelled_tiles[0])

    unlabelled_tiles, unlabelled_positions = sample_cloudy_unlabelled_tiles(swath_tuple, cloud_mask, label_mask, number_of_labels, tile_size)

    return labelled_tiles, unlabelled_tiles, labelled_positions, unlabelled_positions

def read_nc(nc_file, names):
    """return masked arrays, with masks indicating the invalid values"""
    radiances, properties, rois, labels = names
    file = nc4.Dataset(nc_file, 'r', format='NETCDF4')
    #print([x for x in file.variables])
    f_radiances = np.ma.vstack([file.variables[name][:] for name in radiances])
    f_properties = np.ma.vstack([file.variables[name][:] for name in properties])
    f_radiances = f_radiances.filled(-9999)
    f_properties = f_properties.filled(-9999)
    f_rois = file.variables[rois][:]
    f_labels = file.variables[labels][:]
    
    return f_radiances, f_properties, f_rois, f_labels

def height_to_labels_t(labels):
    count = np.zeros([len(labels),8,3,3])
    for i,tile in enumerate(labels):
        for x,y in [[0,0],[0,1],[0,2],[1,0],[1,1],[1,2],[2,0],[2,1],[2,2]]:
            if i==0:
                print(i, tile.shape, tile[:,x,y])        
            count[i,:,x,y] += np.bincount(tile[:,x,y].astype(int)+1,
                                    minlength=9)[1:]
    
    
    return count

def height_to_labels_im(labels):
    count = np.zeros([8,labels.shape[0]* labels.shape[1]])
    print(labels.shape)
    for i,tile in enumerate(labels.reshape(-1,10)):
        if i==0:
            print(i, tile)
        count[:,i] += np.bincount(tile.astype(int)+1,
                                    minlength=9)[1:]
    
    
    return count.reshape(8, labels.shape[0], labels.shape[1])


for daytime in ["daylight/"]:
    nc_dir = os.path.join(work , "temp_cumulo","narval")
    save_dir = os.path.join(work , "CUMULO","narval",daytime)


    
    file_paths = glob.glob(os.path.join(nc_dir, "A2008*"+ day +"*.nc"))
    
    if len(file_paths) == 0:
        print("FILENOTFOUND: ", os.path.join(nc_dir, "A2008*"+ day +"*.nc"))
        continue
    
    for dr in [os.path.join(save_dir, "label/tiles"), 
               os.path.join(save_dir, "unlabel/tiles"),
               os.path.join(save_dir, "label/metadata"),
               os.path.join(save_dir, "unlabel/metadata")]:
        if not os.path.exists(dr):
            os.makedirs(dr)
            
    for filename in file_paths[:]:
       
        try:
            radiances, properties, cloud_mask, labels = read_nc(filename, dicts)
            
            labels_everywhere = height_to_labels_im(np.squeeze(labels.data))
            name = os.path.basename(filename).replace(".nc", "")
            full = np.vstack( (radiances.data, properties.data, cloud_mask.data,labels_everywhere ) )
            np.save(os.path.join(save_dir, name), full.transpose(0,2,1))
            
            
            
            label_mask = get_label_mask(labels)
    
            (labelled_tiles, unlabelled_tiles, labelled_positions,
             unlabelled_positions) = sample_labelled_and_unlabelled_tiles((radiances
                   , properties, cloud_mask, labels), cloud_mask[0], label_mask[0])
    
            labels_by_type = height_to_labels_t(
                np.squeeze(np.transpose(labelled_tiles[3].data,(0,4,2,3,1))) )
           
            
            save_name = os.path.join(save_dir, "label","tiles", name)
            two, three, four, one = (labelled_tiles[1].data, labelled_tiles[2].data,
                labels_by_type, 
                labelled_tiles[0].data)
            stack = np.concatenate((one, two , three, four), axis=1)
            b=0
            for bla in [one, two, three, four]:
                if np.any(np.isnan(bla)):
                    print("nan",filename, bla.shape)
                    b=True
            if b==True:
                break
            if (day != "001") or True:
                np.save(save_name,stack.astype("float16"))# np.stack((labelled_tiles[0].data,
                            #labelled_tiles[1].data, labelled_tiles[2].data, np.squeeze(
                            #np.transpose(labelled_tiles[3].data, (0,4,2,3,1)))),
                            #axis = 1))
            save_name = os.path.join(save_dir, "label", "metadata", name)
            if (day != "001") or True:
                np.save(save_name, labelled_positions.astype("float16"))
        
            save_name = os.path.join(save_dir, "unlabel","tiles", name)
            
            stack = np.concatenate((unlabelled_tiles[0].data, 
                                unlabelled_tiles[1].data, 
                                unlabelled_tiles[2].data), axis =1)
            if day != "001" or True:
                np.save(save_name, stack.astype("float16"))
            save_name = os.path.join(save_dir,  "unlabel", "metadata", name)
            if day != "001" or True:
                np.save(save_name, unlabelled_positions.astype("float16"))

        except Exception as err:
            print(err, "\n"+filename)
            continue
        
