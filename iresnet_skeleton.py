#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 08:20:20 2021

@author: arndt
"""

import torch
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler
import numpy as np
import os
import time


def get_most_frequent_label_tile(labels, single_tile=True):

    if single_tile:

        # if no occurences
        if np.sum(labels) == 0:
            labels = -np.ones(1)

        else:
            class_dist = np.sum(labels, axis=(1, 2))
            labels = [np.argmax(class_dist)]

    else:

        occurrences = np.sum(labels, axis=(2, 3))
        labels = np.argmax(occurrences, axis=1)
        labels[np.sum(occurrences, axis=1) == 0] = -1

    return np.asarray(labels, dtype=np.int8)

class TileDataset(Dataset):
    """ load Cumulo a tile at a time using npy files containing multiple tiles (grouped by swath). 
        Tiles are picked randomly from the entire dataset, not swath-wise"""

    def __init__(self, root_dir, normalizer=None, indices=None, channels = np.arange(13)):
        """
        Args:
            root_dir (string): directory containing the tiles (collected by swath = a file per swath and multiple tiles)
        """
        
        self.root_dir = root_dir
        self.swath_paths = os.listdir(self.root_dir)
        self.channels = channels
        if len(self.swath_paths) == 0:
            print("no npy files in", self.root_dir)
        
        if indices is not None:
            self.swath_paths = [self.swath_paths[i] for i in indices]
        
        self.normalizer = normalizer

    def __len__(self):

        return len(self.swath_paths)

    def __getitem__(self, info):

        if isinstance(info, tuple):

            # load single tile
            swath_idx, tile_idx = info

            swath_path = os.path.join(self.root_dir, self.swath_paths[swath_idx])
            while True:
                try:    
                    data = np.load(swath_path)[tile_idx]
                except IOError:
                    time.sleep(5)
                    continue
                break

        else:

            # load all swath's tiles
            swath_path = os.path.join(self.root_dir, self.swath_paths[info])
            data = np.load(swath_path)

        if len(data.shape) == 3:

            x = data[self.channels]
            labels = get_most_frequent_label_tile(data[-8:], single_tile=True)

        elif len(data.shape) == 4:

            x = data[:, self.channels]
            labels = get_most_frequent_label_tile(data[:, -8:], single_tile=False)

        if self.normalizer is not None:
            x = self.normalizer(x)

        return {"tiles": x, "labels": labels}
    
def tile_collate(swath_tiles):
    
    data = np.vstack([s["tiles"] for s in swath_tiles])
    target = np.hstack([s["labels"] for s in swath_tiles])

    return {"tiles": torch.from_numpy(data), "labels": torch.from_numpy(target)}

def get_dataset_statistics(dataset, nb_classes, use_cuda=True):

    dataloader = torch.utils.data.DataLoader(dataset, batch_size=100, shuffle=False, num_workers=8, collate_fn=tile_collate)
    weights = np.zeros(nb_classes)
    len_c = len(dataset.channels)
    sum_x = torch.zeros(len_c)
    std = torch.zeros(1,len_c , 1, 1)

    if use_cuda:
        sum_x = sum_x.cuda()
        std = std.cuda()
    
    nb_tiles = 0
    for x in dataloader:

        labels, tiles = x["labels"], x["tiles"].float()
        assert not np.any(np.isnan(tiles.numpy()))
        
        nb_tiles += len(tiles)

        if use_cuda:
            tiles = tiles.cuda()

        # class weights
        weights += np.histogram(labels, bins=range(nb_classes+1), normed=False)[0]
        
        sum_x += torch.sum(tiles, (0, 2, 3))
    
    nb_pixels = nb_tiles * 9
    m = (sum_x / nb_pixels).reshape(1, len_c, 1, 1)
    
    for x in dataloader:
        
        tiles = x["tiles"].float()
        assert not np.any(np.isnan(tiles.numpy()))
        if use_cuda:
            tiles = tiles.cuda()

        std += torch.sum((tiles - m).pow(2), (0, 2, 3), keepdim=True)

    s = ((std / nb_pixels)**0.5)
    
    weights /= np.sum(weights)
    weights = 1 / (np.log(1.02 + weights))

    if use_cuda:
        m = m.cpu()
        s = s.cpu()

    return weights / np.sum(weights), m.reshape(len_c, 1, 1).numpy(), s.reshape(len_c, 1, 1).numpy()

class Normalizer(object):

    def __init__(self, mean, std):

        self.mean = mean
        self.std = std

    def __call__(self, image):

        return (image - self.mean) / self.std

def get_tile_sampler(dataset, allowed_idx=None):

    indices = []
    paths = dataset.swath_paths.copy()
    
    if allowed_idx is not None:
        paths = [paths[i] for i in allowed_idx]
    
    for i, swath_name in enumerate(paths):
    
        swath_path = os.path.join(dataset.root_dir, swath_name)
        swath = np.load(swath_path)
        
        indices += [(i, j) for j in range(swath.shape[0])]
    
    return SubsetRandomSampler(indices)


try:
    class_weights = np.load()
    m = np.load()
    s = np.load()

except:
    # load dataset characteristics
    print("Computing dataset mean, standard deviation and class ratios")

    dataset = TileDataset(directroy, channels = parameters) # change to unlabelled di
    class_weights, m, s = get_dataset_statistics(dataset, nb_classes, use_cuda)

    np.save(x, class_weights)
    np.save(x, m)
    np.save(x, s)

normalizer = Normalizer(m, s)
class_weights = torch.from_numpy(class_weights).float()

# get train, validation, test sets by splitting the set of swaths
nb_swaths = len(os.listdir()[:])

idx = np.arange(nb_swaths)
np.random.shuffle(idx)
parameters = np.arange(13)
train_idx, val_idx, test_idx = np.split(idx, [int(.7 * nb_swaths), int(.8 * nb_swaths)])
print("Split:\n Train: {0}, Test: {1}, Vali: {2}".format(len(train_idx), len(test_idx), len(val_idx)))
train_dataset = TileDataset(os.path.join(), normalizer, train_idx, channels = parameters) 
val_dataset = TileDataset(os.path.join(), normalizer, val_idx, channels = parameters) 
test_dataset = TileDataset(os.path.join(), normalizer, test_idx, channels = parameters)
unlabelled_dataset = TileDataset(os.path.join(), normalizer, channels = parameters) 

# samplers
train_sampler = get_tile_sampler(train_dataset)
unlab_train_sampler = get_tile_sampler(unlabelled_dataset)

# data loaders
trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, sampler=train_sampler, num_workers=workers, pin_memory=pim)
unlab_trainloader = torch.utils.data.DataLoader(unlabelled_dataset, batch_size=batch_size, sampler=unlab_train_sampler, num_workers=workers, pin_memory=pim)


valloader = torch.utils.data.DataLoader(val_dataset, batch_size=40, collate_fn=tile_collate, shuffle=False, num_workers=30)
testloader = torch.utils.data.DataLoader(test_dataset, batch_size=40, collate_fn=tile_collate, shuffle=False, num_workers=30)
